#!/bin/bash

# According to https://docs.docker.com/install/linux/docker-ce/ubuntu/

## Set up the repository

### 1. Update the apt package index:

sudo apt-get update

### 2. Install packages to allow apt to use a repository over HTTPS:

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

### 3. Add Docker’s official GPG key:

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

### 4. Set up the stable repository

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

## Install Docker Engine - Community

### 1. Update the apt package index.

sudo apt-get update

### 2. Install the latest version of Docker Engine - Community and containerd

sudo apt-get install -y docker-ce docker-ce-cli containerd.io

## Post-installation steps

sudo usermod -aG docker $USER
