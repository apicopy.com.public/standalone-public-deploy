# ApiCopy.com standalone version Public Deploy

## Requirements

You should open this ports to access installed system:
* 80
* 8080

## Installation

1. Get the setup script

```wget https://gitlab.com/apicopy.com.public/standalone-public-deploy/-/raw/master/setup.sh```

2. Execute setup.sh

```sudo bash ./setup.sh <MACHINE_IP> <NODE_SERIAL> <DB_PASS>```

Where:
* MACHINE_IP - IP Address of your machine where you install product
* NODE_SERIAL - Serial number of your node given you by apicopy
* DB_PASS - any Database password you wish

Example:
```sudo bash ./setup.sh 34.252.216.154 2ac66b87-f685-475b-85ff-5476a5fbdfc9 qweqweqwe1```