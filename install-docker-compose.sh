#!/bin/bash

# According to https://docs.docker.com/compose/install/

## Install Compose on Linux systems

### 1. Run this command to download the current stable release of Docker Compose:

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

### 2. Apply executable permissions to the binary:

sudo chmod +x /usr/local/bin/docker-compose
