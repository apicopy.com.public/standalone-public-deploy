#!/bin/bash

IP=$1
SERIAL=$2
DB_PASS=$3

BASE_URL='https://gitlab.com/apicopy.com.public/standalone-public-deploy/-/raw/master/'

which lsb_release 1>/dev/null 2>&1

if [ $? == 0 ]; then

	if [ "`lsb_release -is`" == 'Ubuntu' ]; then
		wget $BASE_URL/ubuntu/install-docker.sh
		bash ./install-docker.sh
	fi

fi

wget $BASE_URL/install-docker-compose.sh
bash ./install-docker-compose.sh

wget $BASE_URL/docker-compose.yml

wget $BASE_URL/.env

sed -i "s/DEFAULT_IP/${IP}/" .env
sed -i "s/DEFAULT_SERIAL/${SERIAL}/" .env
sed -i "s/DEFAULT_PASS/${DB_PASS}/" .env

wget $BASE_URL/run-platform.sh
bash ./run-platform.sh
