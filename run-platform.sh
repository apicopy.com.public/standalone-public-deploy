#!/bin/bash

docker login -u public-deploy -p nEYa1LKdk7cb8qxAmtH9 registry.gitlab.com

docker pull registry.gitlab.com/apicopy.com/standalone/frontend:latest

docker login -u public-deploy -p TrkSAyic8novXxmMGnhL registry.gitlab.com

docker pull registry.gitlab.com/apicopy.com/standalone/backend:latest
docker pull registry.gitlab.com/apicopy.com/standalone/backend/core/database:latest

docker pull postgres:12.0

docker-compose -p apicopy up -d

source .env

docker run --network=apicopy_platform registry.gitlab.com/apicopy.com/standalone/backend/core/database:latest \
	-url=jdbc:postgresql://postgres:5432/${POSTGRES_DB} \
    -user=${POSTGRES_USER} \
    -password=${POSTGRES_PASSWORD} \
    migrate
